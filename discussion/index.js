const protocol =require ('http');

//identify and designate  a "virtual point" where the network connection will start and  end.

const port = 4000;

// create a connection
protocol.createServer((req, res)=> {
// describe the response when the client interact/communicates with the server.
res.write('Welcome to the Server');
res.end(); // to describe the end of the connection/transmission

}).listen(port)
// we will use the listen() to bind the connection into the designated address.

//createa response in the

console.log(`Server is running on port ${port}`)